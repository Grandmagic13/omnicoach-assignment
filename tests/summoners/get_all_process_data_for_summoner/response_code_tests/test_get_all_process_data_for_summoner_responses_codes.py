import unittest

from hamcrest import *
from parameterized import parameterized

from tests.summoners.functions_summoners import *
from tests.summoners.get_all_process_data_for_summoner.functions_get_all_process_data_for_summoner import \
    get_processes_for_summoner_by_name
from tests.summoners.resources_summoners import *


class GetAllProcessDataForSummonerResponseCodeTests(unittest.TestCase):

    # Test methods

    def test_for_heroKiller777_no_token_response_code(self):
        # Took the liberty of testing for 401 (new requirement). The api spec only states a 200 and a 422 response,
        # but I think 401 could be more appropriate here.
        response = get_processes_for_summoner_by_name(HEROKILLER777_NAME, with_token=False)
        assert_that(response.status_code, is_(requests.codes.unauthorized))

    def test_for_summoner_invalid_name_response_code(self):
        response = get_processes_for_summoner_by_name(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_for_summoner_valid_name_response_code(self, summoner_name):
        response = get_processes_for_summoner_by_name(summoner_name, with_token=True)
        assert_that(response.status_code, is_(requests.codes.ok))
