import unittest

from hamcrest import *
from parameterized import parameterized

from tests.common.custom_matchers.json_schema_matcher import *
from tests.common.custom_matchers.waiting_to_have_item import waits_to_have_item
from tests.summoners.functions_summoners import get_new_process_id_for_summoner, post_processes_summoner
from tests.summoners.get_all_process_data_for_summoner.functions_get_all_process_data_for_summoner import *
from tests.summoners.resources_summoners import *


class GetAllProcessDataForSummonerResponseBodyTests(unittest.TestCase):

    # Test methods

    def test_for_heroKiller777_no_token_response_body(self):
        response = get_processes_for_summoner_by_name(HEROKILLER777_NAME, with_token=False)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("Not authenticated"))

    def test_for_summoner_invalid_name_response_body(self):
        response = get_processes_for_summoner_by_name(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.json(), matches_http_validation_error_schema())

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_for_summoner_valid_name_response_body(self, summoner_name):
        response = get_processes_for_summoner_by_name(summoner_name, with_token=True)
        assert_that(response.json(), matches_process_array_schema())

    def test_if_process_gets_added_to_processes(self):
        valid_new_process_id = get_new_process_id_for_summoner(HEROKILLER777_NAME)
        get_process_ids_for_summoner = {
            "callable": get_processes_ids_for_summoner_by_name,
            "args": [HEROKILLER777_NAME]
        }
        assert_that(get_process_ids_for_summoner, waits_to_have_item(valid_new_process_id))

    def test_if_process_gets_added_to_processes_with_right_contents(self):
        response = post_processes_summoner(HEROKILLER777_NAME, with_token=True)
        new_valid_process_object = response.json()
        get_processes_for_summoner = {
            "callable": get_processes_objects_for_summoner_by_name,
            "args": [HEROKILLER777_NAME]
        }
        assert_that(get_processes_for_summoner, waits_to_have_item(new_valid_process_object))