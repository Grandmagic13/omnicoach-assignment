from tests.summoners.functions_summoners import summoner_processes_url_by_name, summoner_get_request


def get_processes_for_summoner_by_name(name, with_token):
    summoner_process_url = summoner_processes_url_by_name(name)
    return summoner_get_request(summoner_process_url, with_token)


def get_processes_ids_for_summoner_by_name(name):
    response = get_processes_for_summoner_by_name(name, with_token=True)
    return [item["id"] for item in response.json()]


def get_processes_objects_for_summoner_by_name(name):
    return get_processes_for_summoner_by_name(name, with_token=True).json()
