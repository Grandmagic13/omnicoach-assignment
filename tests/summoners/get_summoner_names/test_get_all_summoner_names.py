import unittest

import requests
from hamcrest import *

from tests.summoners.resources_summoners import *


class GetAllSummonerNamesTests(unittest.TestCase):

    # Helper methods

    @staticmethod
    def get_summoner_names():
        return requests.get(SUMMONERS_URL)

    # Test methods

    def test_response_code(self):
        response = self.get_summoner_names()
        assert_that(response.status_code, is_(requests.codes.ok))

    def test_response_body(self):
        response = self.get_summoner_names()
        summoner_names_list = list(SUMMONER_NAMES)
        assert_that(response.json(), equal_to(summoner_names_list))
