import unittest

from hamcrest import *
from parameterized import parameterized

from tests.common.custom_matchers.json_schema_matcher import matches_http_validation_error_schema
from tests.summoners.resources_summoners import *
from tests.summoners.validate_summoner_names.functions_validate_summoner_names import *


class ValidateSummonerNameResponseBodyTests(unittest.TestCase):

    # Test methods

    def test_validate_heroKiller777_no_token(self):
        response = get_validate_summoner_name(HEROKILLER777_NAME, with_token=False)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("Not authenticated"))

    def test_validate_empty_summoner_name(self):
        response = get_validate_summoner_name(EMPTY_SUMMONER_NAME, with_token=True)
        assert_that(response.json(), matches_http_validation_error_schema())

    def test_validate_invalid_summoner_name(self):
        response = get_validate_summoner_name(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.json(), matches_http_validation_error_schema())

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_validate_valid_summoner_name(self, summoner_name):
        response = get_validate_summoner_name(summoner_name, with_token=True)
        assert_that(response.json(), equal_to(VALID_SUMMONER_EXPECTED_RESPONSE_BODY))
