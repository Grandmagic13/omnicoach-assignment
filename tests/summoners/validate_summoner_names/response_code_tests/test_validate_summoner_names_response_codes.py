import unittest

import requests
from hamcrest import *
from parameterized import parameterized

from tests.summoners.resources_summoners import *
from tests.summoners.validate_summoner_names.functions_validate_summoner_names import *


class ValidateSummonerNameResponseCodeTests(unittest.TestCase):

    # Test methods

    def test_validate_heroKiller777_no_token(self):
        # Took the liberty of testing for 401 (new requirement). The api spec only states a 200 and a 422 response,
        # but I think 401 could be more appropriate here.
        response = get_validate_summoner_name(HEROKILLER777_NAME, with_token=False)
        assert_that(response.status_code, is_(requests.codes.unauthorized))

    def test_validate_empty_summoner_name(self):
        response = get_validate_summoner_name(EMPTY_SUMMONER_NAME, with_token=True)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_validate_invalid_summoner_name(self):
        response = get_validate_summoner_name(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_validate_valid_summoner_name(self, summoner_name):
        response = get_validate_summoner_name(summoner_name, with_token=True)
        assert_that(response.status_code, is_(requests.codes.ok))
