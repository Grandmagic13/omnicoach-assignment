from tests.summoners.functions_summoners import summoner_get_request, summoner_url_by_name


def get_validate_summoner_name(name, with_token):
    summoner_url = summoner_url_by_name(name)
    return summoner_get_request(summoner_url, with_token)