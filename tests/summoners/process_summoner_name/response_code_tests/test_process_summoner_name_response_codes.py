import unittest

from hamcrest import *
from parameterized import parameterized

from tests.summoners.functions_summoners import *
from tests.summoners.functions_summoners import post_processes_summoner
from tests.summoners.resources_summoners import *


class ProcessSummonerNameTests(unittest.TestCase):

    # Test methods

    def test_for_heroKiller777_no_token(self):
        # Took the liberty of testing for 401 (new requirement). The api spec only states a 200 and a 422 response,
        # but I think 401 could be more appropriate here.
        response = post_processes_summoner(HEROKILLER777_NAME, with_token=False)
        assert_that(response.status_code, is_(requests.codes.unauthorized))

    def test_for_summoner_invalid_name(self):
        response = post_processes_summoner(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_empty_name(self):
        response = post_processes_summoner("", with_token=True)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_for_summoner_valid_name(self, summoner_name):
        response = post_processes_summoner(summoner_name, with_token=True)
        assert_that(response.status_code, is_(requests.codes.ok))
