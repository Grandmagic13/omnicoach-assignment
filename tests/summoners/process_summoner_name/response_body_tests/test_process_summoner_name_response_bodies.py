import unittest
from datetime import datetime

import now as now
from hamcrest import *
from parameterized import parameterized
from waiting import wait

from tests.common.custom_matchers.json_schema_matcher import *
from tests.summoners.functions_summoners import post_processes_summoner, get_new_process_id_for_summoner
from tests.summoners.resources_summoners import *


class ProcessSummonerNameTests(unittest.TestCase):

    # Helper methods

    @staticmethod
    def wait_until_time_seconds_are_less_than_50():
        if datetime.now().time().second > 50:
            wait(lambda: datetime.now().time().second < 50)

    # Test methods

    def test_for_heroKiller777_no_token(self):
        response = post_processes_summoner(HEROKILLER777_NAME, with_token=False)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("Not authenticated"))

    def test_for_summoner_invalid_name(self):
        response = post_processes_summoner(INVALID_SUMMONER_NAME, with_token=True)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("not found"))

    def test_for_summoner_empty_name(self):
        response = post_processes_summoner("", with_token=True)
        assert_that(response.json(), matches_http_validation_error_schema())

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_for_summoner_valid_name(self, summoner_name):
        response = post_processes_summoner(summoner_name, with_token=True)
        assert_that(response.json(), matches_process_schema())

    def test_if_ids_are_unique(self):
        id_1 = get_new_process_id_for_summoner(HEROKILLER777_NAME)
        id_2 = get_new_process_id_for_summoner(HEROKILLER777_NAME)
        assert_that(id_1, not_(equal_to(id_2)))

    def test_date_generation(self):
        self.wait_until_time_seconds_are_less_than_50()
        minute_of_request_creation = datetime.now().strftime('%Y-%m-%dT%H:%M')
        response = post_processes_summoner(HEROKILLER777_NAME, with_token=True)
        process_created_date_time = response.json()["created"]
        assert_that(process_created_date_time, contains_string(minute_of_request_creation))

    @parameterized.expand(list(SUMMONER_NAMES[:2]))
    def test_summoner_name_creation(self, input_name):
        # Testing for two different summoners to make sure that no one summoner name is hardcoded

        response = post_processes_summoner(input_name, with_token=True)
        summoner_name = response.json()["summoner_name"]
        assert_that(summoner_name, is_(input_name))
