import unittest

from hamcrest import *
from parameterized import parameterized

from tests.common.custom_matchers.waiting_for_status_code_matcher import response_code_waits_to_be_
from tests.summoners.functions_summoners import *
from tests.summoners.resources_summoners import *


class GetProcessDataForIdResponseCodeTests(unittest.TestCase):

    # Test methods

    # TODO produce 204 still processing state

    def test_for_summoner_invalid_name(self):
        valid_new_process_id = get_new_process_id_for_summoner(HEROKILLER777_NAME)
        response = get_process_for_summoner_by_id(INVALID_SUMMONER_NAME, valid_new_process_id)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_invalid_id(self):
        response = get_process_for_summoner_by_id(HEROKILLER777_NAME, INVALID_PROCESS_ID)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_invalid_all(self):
        response = get_process_for_summoner_by_id(INVALID_SUMMONER_NAME, INVALID_PROCESS_ID)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_empty_name(self):
        valid_new_process_id = get_new_process_id_for_summoner(HEROKILLER777_NAME)
        response = get_process_for_summoner_by_id(EMPTY_SUMMONER_NAME, valid_new_process_id)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_empty_id(self):
        response = get_process_for_summoner_by_id(HEROKILLER777_NAME, EMPTY_PROCESS_ID)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_for_summoner_empty_all(self):
        response = get_process_for_summoner_by_id(EMPTY_SUMMONER_NAME, EMPTY_PROCESS_ID)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    @parameterized.expand(list(SUMMONER_NAMES))
    def test_for_summoner_valid_values(self, summoner_name):
        valid_new_process_id = get_new_process_id_for_summoner(summoner_name)
        get_process_for_summoner_with_parameters = {
            "callable": get_process_for_summoner_by_id,
            "args": [summoner_name, valid_new_process_id]
        }
        assert_that(get_process_for_summoner_with_parameters, response_code_waits_to_be_(requests.codes.ok))
