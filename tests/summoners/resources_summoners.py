from tests.common.resources_common import BASE_URL

SUMMONERS_URL = BASE_URL + "/summoners/"
PROCESS_SUMMONER_URL = SUMMONERS_URL + "process"
INVALID_SUMMONER_NAME = "N00bSaiB0t"
EMPTY_SUMMONER_NAME = " "
EMPTY_PROCESS_ID = " "
INVALID_PROCESS_ID = "fe4r-th3-n00b"
SUMMONER_NAMES = (
    "heroKiller777",
    "pwnerZed",
    "iAmAWhoIAm",
    "naby",
    "et1337"
)
HEROKILLER777_NAME = SUMMONER_NAMES[0]
VALID_SUMMONER_EXPECTED_RESPONSE_BODY = {
    "msg": "valid"
}

PROCESS_VALIDATION_SCHEMA = {
    "type": "object",
    "required": ["created", "summoner_name", "id"],
    "properties": {
        "created": {
            "type": "string",
            "format": "date-time"
        },
        "summoner_name": {
            "type": "string",
        },
        "id": {
            "type": "string",
            "format": "uuid"
        }
    }
}

PROCESS_ARRAY_VALIDATION_SCHEMA = {
    "type": "array",
    "items": PROCESS_VALIDATION_SCHEMA
}