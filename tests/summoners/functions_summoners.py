import requests

from tests.common.functions_common import post_access_request
from tests.common.resources_common import *
from tests.summoners.resources_summoners import SUMMONERS_URL, PROCESS_SUMMONER_URL


def generate_authorization_headers():
    response = post_access_request(VALID_USERNAME, VALID_PASSWORD)
    token = response.json()["access_token"]
    bearer_token_string = "Bearer {0}".format(token)
    return {"Authorization": bearer_token_string}


def summoner_get_request(request_url, with_token):
    return summoner_request(requests.get, request_url, with_token)


def summoner_post_request(request_url, with_token, query_params):
    return summoner_request(requests.post, request_url, with_token, query_params)


def summoner_request(request_method, request_url, with_token, query_params=None):
    return request_method(request_url, headers=generate_authorization_headers(), params=query_params) if with_token \
        else request_method(request_url, params=query_params)


def summoner_url_by_name(name):
    return SUMMONERS_URL + name


def summoner_processes_url_by_name(name):
    return SUMMONERS_URL + name + "/process"


def summoner_process_url_by_name_and_id(name, id):
    return summoner_processes_url_by_name(name) + "/" + id


def post_processes_summoner(name, with_token):
    query_params = {"summoner_name": name}
    return summoner_post_request(PROCESS_SUMMONER_URL, with_token, query_params)


def get_new_process_id_for_summoner(name):
    response = post_processes_summoner(name, with_token=True)
    return response.json()["id"]


def get_process_for_summoner_by_id(name, id):
    summoner_process_url = summoner_process_url_by_name_and_id(name, id)
    return summoner_get_request(summoner_process_url, with_token=False)