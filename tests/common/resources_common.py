WAITING_MATCHERS_TIMEOUT = 15
BASE_URL = "http://bastion.dev.omnicoach.gg:8001/api/v1"
ACCESS_TOKEN_URL = BASE_URL + "/login/access-token"
VALID_USERNAME = "qa@omnicoach.gg"
VALID_PASSWORD = "pass"
HTTP_VALIDATION_ERROR_SCHEMA = {
    "type": "object",
    "required": ["detail"],
    "properties": {
        "detail": {
            "type": "array",
            "items": {
                "type": "object",
                "required": ["loc", "msg", "type"],
                "properties": {
                    "loc": {
                        "type": "array",
                        "minItems": 1,
                        "items": {
                            "type": "string"
                        }
                    },
                    "msg": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    }
                }
            }
        }
    }
}
PROCESS_RESULT_BASE_SCHEMA = {
    "type": "object",
    "required": ["process_id", "summoner_data"],
    "properties": {
        "process_id": {
            "type": "string",
            "format": "uuid"
        },
        "summoner_name":{
            "type": "string"
        },
        "summoner_data": {
            "type": "object"
        }
    }
}
