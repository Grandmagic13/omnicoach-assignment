import time
from typing import Union, Sequence

from hamcrest.core.helpers.wrap_matcher import wrap_matcher
from hamcrest.core.matcher import Matcher, T
from hamcrest.library.collection.issequence_containing import IsSequenceContaining

from tests.common.resources_common import WAITING_MATCHERS_TIMEOUT


class WaitsToHaveItem(IsSequenceContaining):

    def __init__(self, element_matcher: Matcher[T]) -> None:
        self.element_matcher = element_matcher

    def _matches(self, callable_with_parameters) -> bool:
        pollable = callable_with_parameters["callable"]
        args = callable_with_parameters["args"]
        self.sequence_matcher = IsSequenceContaining(self.element_matcher)
        if not self.sequence_matcher._matches(pollable(*args)):
            self.wait_until_process_request_would_be_done()
        self.actual_list = pollable(*args)
        return self.sequence_matcher._matches(pollable(*args))

    def wait_until_process_request_would_be_done(self):
        # Wanted to use wait here, but the huge list probably overloads the wait library's call stack so much that it's
        # actually more efficient to just wait out the timeout and retry just one more time.
        time.sleep(WAITING_MATCHERS_TIMEOUT)

    def describe_mismatch(self, callable_with_parameters, description):
        self.sequence_matcher.describe_mismatch(self.actual_list, description)


def waits_to_have_item(*items: Union[Matcher[T], T]) -> Matcher[Sequence[T]]:
    matchers = []
    for item in items:
        matchers.append(wrap_matcher(item))
    return WaitsToHaveItem(*matchers)