from hamcrest.core.core.isequal import IsEqual
from waiting import wait, TimeoutExpired

from tests.common.resources_common import WAITING_MATCHERS_TIMEOUT


class StatusCodeWaitsToBeEqualTo(IsEqual):

    def __init__(self, obj):
        self.object = obj

    def _matches(self, callable_with_parameters) -> bool:
        request = callable_with_parameters["callable"]
        args = callable_with_parameters["args"]
        timeout = WAITING_MATCHERS_TIMEOUT
        try:
            result = wait(lambda: request(*args).status_code == self.object, timeout_seconds=timeout)
            self.actual_status_code = request(*args).status_code
            return result
        except TimeoutExpired:
            self.actual_status_code = request(*args).status_code
            self.error_message = "Timeout expired ({0} seconds)".format(timeout)
            return False

    def describe_mismatch(self, callable_with_parameters, description):
        if self.error_message is not None:
            description.append_text(self.error_message + " and ")
        description.append_text("was <" + str(self.actual_status_code) + ">")


def response_code_waits_to_be_(obj):
    return StatusCodeWaitsToBeEqualTo(obj)
