import jsonschema
from hamcrest.core.base_matcher import BaseMatcher
from jsonschema import ValidationError
from waiting import wait, TimeoutExpired

from tests.common.custom_matchers.json_schema_matcher import MatchesJSONSchema
from tests.common.resources_common import HTTP_VALIDATION_ERROR_SCHEMA, WAITING_MATCHERS_TIMEOUT, \
    PROCESS_RESULT_BASE_SCHEMA
from tests.summoners.resources_summoners import *


class WaitToMatchJSONSchema(MatchesJSONSchema):

    def __init__(self, schema):
        self.schema = schema
        self.actual_json = None
        self.timeout_message = None


    def get_request_json(self):
        return self.request(*self.args).json()

    def _matches(self, callable_with_parameters):
        self.request = callable_with_parameters["callable"]
        self.args = callable_with_parameters["args"]
        timeout = WAITING_MATCHERS_TIMEOUT
        try:
            self.matcher = MatchesJSONSchema(self.schema)
            result = wait(lambda: self.matcher._matches(self.get_request_json()), timeout_seconds=timeout)
            self.actual_json = self.get_request_json()
            return result
        except TimeoutExpired:
            self.actual_json = self.get_request_json()
            self.timeout_message = "Timeout expired ({0} seconds)".format(timeout)
            return False

    def describe_to(self, description):
        description.append_description_of(self.matcher)

    def describe_mismatch(self, json_object, description):
        if self.timeout_message is not None:
            description.append_text(self.timeout_message + " and ")
        self.matcher.describe_mismatch(self.actual_json, description)


def waits_to_match_process_result_base_schema():
    return WaitToMatchJSONSchema(PROCESS_RESULT_BASE_SCHEMA)
