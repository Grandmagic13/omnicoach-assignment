import jsonschema
from hamcrest.core.base_matcher import BaseMatcher
from jsonschema import ValidationError

from tests.common.resources_common import HTTP_VALIDATION_ERROR_SCHEMA
from tests.summoners.resources_summoners import *


class MatchesJSONSchema(BaseMatcher):

    def __init__(self, schema):
        self.schema = schema

    def _matches(self, json_object):
        try:
            jsonschema.validate(json_object, self.schema, format_checker=jsonschema.FormatChecker())
            return True
        except ValidationError as validation_error:
            self.error_message = validation_error.message
            return False

    def describe_to(self, description):
        description.append_text("JSON object valid based on schema:\n" + str(self.schema))

    def describe_mismatch(self, json_object, description):
        description.append_text("was " + str(json_object)) \
            .append_text("\nValidationError: " + self.error_message)


def matches_http_validation_error_schema():
    return MatchesJSONSchema(HTTP_VALIDATION_ERROR_SCHEMA)


def matches_process_array_schema():
    return MatchesJSONSchema(PROCESS_ARRAY_VALIDATION_SCHEMA)


def matches_process_schema():
    return MatchesJSONSchema(PROCESS_VALIDATION_SCHEMA)
