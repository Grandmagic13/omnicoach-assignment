import requests

from tests.common.resources_common import *


def post_access_request(username, password):
    request_body = {'username': username, 'password': password}
    return requests.post(ACCESS_TOKEN_URL, data=request_body)
