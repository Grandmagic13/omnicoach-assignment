import unittest

from hamcrest import *

from tests.common.functions_common import post_access_request
from tests.common.custom_matchers.json_schema_matcher import matches_http_validation_error_schema
from tests.login.resources_login import *
from tests.common.resources_common import *


class LoginResponseBodyTests(unittest.TestCase):

    # Test methods

    def test_wrong_username(self):
        response = post_access_request(INVALID_USERNAME, VALID_PASSWORD)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("Incorrect email or password"))

    def test_wrong_password(self):
        response = post_access_request(VALID_USERNAME, INVALID_PASSWORD)
        assert_that(response.json(), matches_http_validation_error_schema())
        # the two assertions could be merged together if I had an exact example of the correct response body
        assert_that(response.text, contains_string("Incorrect email or password"))

    def test_valid_credentials(self):
        response = post_access_request(VALID_USERNAME, VALID_PASSWORD)
        response_content = response.json()
        assert_that(response_content, has_entry("token_type", "bearer"))
        assert_that(response_content, has_key("access_token"))

    def test_empty_username(self):
        response = post_access_request("", VALID_PASSWORD)
        assert_that(response.json(), equal_to(EMPTY_USERNAME_EXPECTED_RESPONSE_BODY))

    def test_empty_password(self):
        response = post_access_request(VALID_USERNAME, "")
        assert_that(response.json(), equal_to(EMPTY_PASSWORD_EXPECTED_RESPONSE_BODY))

    def test_empty_credentials(self):
        response = post_access_request("", "")
        assert_that(response.json(), equal_to(EMPTY_CREDENTIALS_EXPECTED_RESPONSE_BODY))
