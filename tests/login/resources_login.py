INVALID_USERNAME = "invalidUser@n00b.orz"
INVALID_PASSWORD = "1nvalid"
EMPTY_USERNAME_EXPECTED_RESPONSE_BODY = {
  "detail": [
    {
      "loc": [
        "body",
        "username"
      ],
      "msg": "field required",
      "type": "value_error.missing"
    }
  ]
}
EMPTY_PASSWORD_EXPECTED_RESPONSE_BODY = {
  "detail": [
    {
      "loc": [
        "body",
        "password"
      ],
      "msg": "field required",
      "type": "value_error.missing"
    }
  ]
}
EMPTY_CREDENTIALS_EXPECTED_RESPONSE_BODY = {
  "detail": [
    EMPTY_USERNAME_EXPECTED_RESPONSE_BODY["detail"][0],
    EMPTY_PASSWORD_EXPECTED_RESPONSE_BODY["detail"][0]
  ]
}
