import unittest

from hamcrest import *
import requests

from tests.common.functions_common import post_access_request
from tests.login.resources_login import *
from tests.common.resources_common import *


class LoginResponseCodesTests(unittest.TestCase):

    # Test methods

    def test_wrong_username(self):
        # Took the liberty of testing for 401 (new requirement). The api spec only states a 200 and a 422 response,
        # but I think 401 could be more appropriate here.
        response = post_access_request(INVALID_USERNAME, VALID_PASSWORD)
        assert_that(response.status_code, is_(requests.codes.unauthorized))

    def test_wrong_password(self):
        # Took the liberty of testing for 401 (new requirement). The api spec only states a 200 and a 422 response,
        # but I think 401 could be more appropriate here.
        response = post_access_request(VALID_USERNAME, INVALID_PASSWORD)
        assert_that(response.status_code, is_(requests.codes.unauthorized))

    def test_valid_credentials(self):
        response = post_access_request(VALID_USERNAME, VALID_PASSWORD)
        assert_that(response.status_code, is_(requests.codes.ok))

    def test_empty_username(self):
        response = post_access_request("", VALID_PASSWORD)
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_empty_password(self):
        response = post_access_request(VALID_USERNAME, "")
        assert_that(response.status_code, is_(requests.codes.unprocessable))

    def test_empty_credentials(self):
        response = post_access_request("", "")
        assert_that(response.status_code, is_(requests.codes.unprocessable))
