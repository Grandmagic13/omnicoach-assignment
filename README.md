# OmniCoach Interview Assignment Testing Framework
Welcome to the homework assignment test framework repo!

## Usage:

### Local:
- For running the test framework locally on a windows machine, as I used it during local development, please use the
`local_runner.bat` file, as it retains output coloring.
- If you want to run it on a linux machine, or just want a quick one-time run, please use the `local_runner.py` script!
- To run individual test suite files, please provide test folder relative path to any of the two scripts (e.g. 
`local_runner.bat test/login/response_body_tests`)
- See reports folder for generated HTML reports 

### CI/CD runs:
- Test job runs automatically in response to new git pushes to branches & pull requests.
- Test jobs can also be manually started through the side-menu through *CI / CD > Pipelines > 'Run Pipeline'* button on
pipelines page (the page will prompt you to choose a branch or commit to run the job against. If you just want a quick
run, please hit the 'Run Pipeline' button again to start the test job)
- Test results can be viewed through clicking on the job number (*e.g. \#147216287*) on the pipelines dashboard
(*CI / CD > Pipelines*) and clicking on the *Tests* tab
(See https://gitlab.com/Grandmagic13/omnicoach-assignment/pipelines/147216287/test_report for example)

## Developer notes

### Response codes
Several undocumented response codes have been found during testing (e.g. `401` instead of `422`), some of
which I found to be more appropriate as requirements than what the documentation provided. Strictly speaking, I am aware
that I could have just made some test cases fail by requiring the documented responses but for this assignment I thought
that testing for the more appropriate responses may better highlight my opinion about the api design and where I would
recommend to take it in the future.

### Curiosities
Locally generated reports from interesting bugs have been documented under the `interesting_error_reports` folder

### Cases untested
I wasn't able to produce the 204 response (*still processing*) for the process data query for the
`http://bastion.dev.omnicoach.gg:8001/api/v1/summoners/{summoner_name}/process/{process_id}` endpoint. It always seemed
to immediately return with the process data, even after trying to stress the endpoint with thread executor.