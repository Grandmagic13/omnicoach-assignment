import os
import sys
from distutils.dir_util import copy_tree
from pathlib import Path
from time import strftime

import pytest

timestamp = strftime("%Y-%m-%d_%H-%M-%S")
timestamp_folder_path = "reports/{0}/".format(timestamp)
Path(timestamp_folder_path).mkdir(parents=True, exist_ok=True)
options = ["--html", "{0}report.html".format(timestamp_folder_path)]
if len(sys.argv) > 1:
    options = [sys.argv[1]] + options
pytest.main(options)
copy_tree(src=timestamp_folder_path, dst="reports/latest/")
