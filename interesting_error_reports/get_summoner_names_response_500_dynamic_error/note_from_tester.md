Caught this interesting bug during local runs in `test_get_summoner_names_response_body` and also during
`test_validate_valid_summoner_name_response_code`. Seems dynamic at first glance.
Please see *report.html*s in the timestamped folders for more details.

```
self = <tests.summoners.test_summoners.SummonerTests testMethod=test_get_summoner_names_response_code>

    def test_get_summoner_names_response_code(self):
        response = self.get_summoner_names()
>       assert_that(response.status_code, is_(requests.codes.ok))
E       AssertionError:
E       Expected: <200>
E            but: was <500>

tests\summoners\test_summoners.py:31: AssertionError

```
```

self = <tests.summoners.test_summoners.SummonerTests testMethod=test_validate_valid_summoner_name_response_code>

    def test_validate_valid_summoner_name_response_code(self):
        response = self.get_validate_heroKiller777(with_token=True)
>       assert_that(response.status_code, is_(requests.codes.ok))
E       AssertionError:
E       Expected: <200>
E            but: was <500>

tests\summoners\test_summoners.py:92: AssertionError

```
