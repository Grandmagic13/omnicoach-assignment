Caught this interesting bug during local runs in `test_get_summoner_names_response_body`. Seems dynamic at
first glance. Please see *report.html*s in the timestamped folders for more details.

```
self = <tests.summoners.test_summoners.SummonerTests testMethod=test_get_summoner_names_response_body>

    def test_get_summoner_names_response_body(self):
        response = self.get_summoner_names()
        summoner_names_list = list(SUMMONER_NAMES)
>       assert_that(response.json(), equal_to(summoner_names_list))
E       AssertionError:
E       Expected: <['heroKiller777', 'pwnerZed', 'iAmAWhoIAm', 'naby', 'et1337']>
E            but: was <{'detail': 'This should never run!!!'}>

tests\summoners\test_summoners.py:32: AssertionError
```
